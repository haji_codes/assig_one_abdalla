import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class DialPadClone extends StatelessWidget {
  const DialPadClone({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color.fromARGB(255, 98, 181, 230),
      body: Column(
        children: [
          _Header(),
          SizedBox(height: 100,),
          Center(child: Text('123456',style: TextStyle(color: Colors.white, fontSize: 30),)),
          Spacer(),
        _Actions()
          
        ],
      ),
    );
  }
}


class _Header extends StatelessWidget {
  const _Header({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
            padding: const EdgeInsets.all(15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  MdiIcons.filePhone,
                  size: 15,
                  color: const Color.fromARGB(255, 12, 61, 102),
                ),
                const SizedBox(
                  width: 8,
                ),
                Icon(
                  MdiIcons.phone,
                  size: 15,
                  color: Colors.white,
                ),
                const SizedBox(
                  width: 8,
                ),
                const Text(
                  '00:50',
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
          );
  }
}

class _Actions extends StatelessWidget {
  const _Actions({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
            margin: const EdgeInsets.only(bottom: 50),
            padding: const EdgeInsets.all(40),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20), color: Colors.white),
            height: 350,
            width: 0.8 * MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _ActionIcons(iconName: MdiIcons.plus,label: 'Add call'),
                      _ActionIcons(iconName: MdiIcons.pause,label: 'Hold call'),
                      _ActionIcons(iconName: MdiIcons.bluetooth,label: 'Bluetooth')
                    ]),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _ActionIcons(iconName: MdiIcons.volumeHigh,label: 'Speaker'),
                      _ActionIcons(iconName: MdiIcons.microphoneOff,label: 'Mute'),
                      _ActionIcons(iconName: MdiIcons.dialpad,label: 'Keypad'),
                    ]),
                const SizedBox(
                  height: 5,
                ),
                Center(
                  child: Container(
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: Colors.red),
                    height: 60,
                    width: 60,
                    child: Icon(
                      MdiIcons.phoneHangup,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          );
  }
}

class _ActionIcons extends StatelessWidget {
  final IconData iconName;
  final String label;

  const _ActionIcons({required this.iconName, required this.label});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          iconName,
          size: 40,
        ),
        const SizedBox(
          height: 5,
        ),
        Text(label)
      ],
    );
  }
}
