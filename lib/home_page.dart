import 'package:assig_one_abdalla/screen_android_calling.dart';
import 'package:assig_one_abdalla/screen_fb_clone.dart';
import 'package:flutter/material.dart';





class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const DialPadClone()),
                );
              },
              child: const Text('  Calling Screen  '),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const FbCloneScreen()),
                );
              },
              child: const Text('FB Clone Screen'),
            ),
          ],
        ),
      ),
    );
  }
}