import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class Header extends StatelessWidget {
  const Header({super.key});

  @override
  Widget build(BuildContext context) {

    return  SliverAppBar(
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          title:const Text(
              'facebook',
              style: TextStyle(
                color: Color(0xFF1777F2),
                fontSize: 28,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.5
                ),
            ),
            actions: [
              const Padding(
                padding: EdgeInsets.all(5.0),
                child: Icon(Icons.search),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: Icon(MdiIcons.facebookMessenger),
              ),
              ],
      );
    
  }
}
