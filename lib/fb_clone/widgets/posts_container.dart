import 'package:assig_one_abdalla/fb_clone/data/data_export.dart';
import 'package:assig_one_abdalla/fb_clone/models/models_export.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class PostContainer extends StatefulWidget {
  const PostContainer({super.key});

  @override
  State<PostContainer> createState() => _PostContainerState();
}

class _PostContainerState extends State<PostContainer> {
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(childCount: posts.length,
          (context, index) {
        final Post post = posts[index];
        final crntPostImgUri = post.haveImage ? post.postImgUri : null;
        return Container(
          margin: const EdgeInsets.only(top: 6.0,),
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          color: Colors.white,
          child: 
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, 
              children: 
              [
                _PostHeader(post: post),
                const SizedBox(height: 12.0),
                Text(post.text),
                post.haveImage ? const SizedBox(height: 5.0,) : const SizedBox.shrink(),
                crntPostImgUri != null ? Image.asset(crntPostImgUri) : const SizedBox.shrink(),
                const SizedBox( height: 4.0, ),
                _PostStats(post: post),
                const Divider(thickness: 0.5,),
                _PostActions(
                  post: post,
                  likedFunc: (){setState(() {
                    post.likes = post.liked ? post.likes-1 : post.likes+1;
                    post.liked = !post.liked;
                  });},)
              ]),
          ),
        );
      }),
    );
  }
}

class _PostHeader extends StatelessWidget {
  final Post post;
  const _PostHeader({required this.post});

  @override
  Widget build(BuildContext context) {
    var privacyIcon = post.privacy ? Icons.lock : Icons.public;
    return Row(
      children: [
        CircleAvatar(
            radius: 20,
            backgroundColor: Colors.grey,
            backgroundImage: AssetImage(post.user.imgUri)),
        const SizedBox(
          width: 8,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                post.user.name,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              Row(
                children: [
                  Text(
                    '${post.timeAgo} · ',
                    style: TextStyle(color: Colors.grey[600], fontSize: 12),
                  ),
                  Icon(
                    privacyIcon,
                    size: 12,
                  )
                ],
              )
            ],
          ),
        ),
        IconButton(onPressed: () {}, icon: const Icon(Icons.more_horiz))
      ],
    );
  }
}

class _PostStats extends StatelessWidget {
  final Post post;
  const _PostStats({required this.post});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              padding: const EdgeInsets.all(4.0),
              decoration: const BoxDecoration(
                  color: Color(0xFF1777F2), shape: BoxShape.circle),
              child: const Icon(
                Icons.thumb_up,
                size: 10.0,
                color: Colors.white,
              ),
            ),
            const SizedBox(
              width: 3.0,
            ),
            Expanded(
                child: Text(
              '${post.likes}',
              style: TextStyle(color: Colors.grey[600]),
            )),
            Text(
              '${post.comments} Comments',
              style: TextStyle(color: Colors.grey[600]),
            ),
            const SizedBox(
              width: 9.0,
            ),
            Text(
              '${post.shares} Shares',
              style: TextStyle(color: Colors.grey[600]),
            ),
          ],
        )
      ],
    );
  }
}

class _PostActions extends StatelessWidget {
  final Post post;
  final Function likedFunc;
  const _PostActions({required this.post, required this.likedFunc, });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        TextButton(
          onPressed: () {likedFunc();},
          child: Row(
            children: [
              Icon(
                !post.liked ? MdiIcons.thumbUpOutline : MdiIcons.thumbUp,
                color: !post.liked ? Colors.black : Colors.blue,
              ),
              const SizedBox(
                width: 5.0,
              ),
              Text(
                'Like',
                style: TextStyle(color: !post.liked ? Colors.black : Colors.blue),
              ),
            ],
          ),
        ),
        TextButton(
          onPressed: () {},
          child: Row(
            children: [
              Icon(
                MdiIcons.commentOutline,
                color: Colors.grey[600],
              ),
              const SizedBox(
                width: 5.0,
              ),
              const Text(
                'Comment',
                style: TextStyle(color: Colors.black),
              ),
            ],
          ),
        ),
        TextButton(
          onPressed: () {},
          child: Row(
            children: [
              Icon(
                MdiIcons.shareOutline,
                color: Colors.grey[600],
              ),
              const SizedBox(
                width: 5.0,
              ),
              const Text(
                'Share',
                style: TextStyle(color: Colors.black),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
