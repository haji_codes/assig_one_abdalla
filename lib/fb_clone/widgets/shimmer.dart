import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingShimmer extends StatelessWidget {
  const LoadingShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        automaticallyImplyLeading: false,
        // backgroundColor: Colors.white,
        title: const Text('facebook',
            style: TextStyle(
                color: Color(0xFF1777F2),
                fontSize: 28,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.5)),
      ),
      body: Shimmer.fromColors(
          baseColor: Colors.grey.shade400,
          highlightColor: Colors.green.shade100,
          direction: ShimmerDirection.ltr,
          child: const Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CircleAvatar(),
                    CircleAvatar(),
                    CircleAvatar(),
                    CircleAvatar(),
                    CircleAvatar(),
                    CircleAvatar(),
                  ],
                ),
              ),
              _ShimmerContainer1(),
              _ShimmerContainer2(),
            ],
          )),
    );
  }
}

class _ShimmerContainer extends StatelessWidget {
  const _ShimmerContainer();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 15,),
                width: 0.6 * MediaQuery.of(context).size.width,
                height: 20,
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(20)),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                margin:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                width: 0.8 * MediaQuery.of(context).size.width,
                height: 20,
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(20)),
              ),
            ],
          ),
          Container(
            width: 0.9 * MediaQuery.of(context).size.width,
            height: 0.3 * MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                color: Colors.grey, borderRadius: BorderRadius.circular(20)),
          ),
        ],
      ),
    );
  }
}


class _ShimmerContainer1 extends StatelessWidget {
  const _ShimmerContainer1();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        children: [
          
          Container(
            width: 0.9 * MediaQuery.of(context).size.width,
            height: 0.3 * MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                color: Colors.grey, borderRadius: BorderRadius.circular(20)),
          ),
          Row(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                width: 0.8 * MediaQuery.of(context).size.width,
                height: 20,
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(20)),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 15),
                width: 0.6 * MediaQuery.of(context).size.width,
                height: 20,
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(20)),
              ),
            ],
          ),
        ],
      ),
    );
  }
}


class _ShimmerContainer2 extends StatelessWidget {
  const _ShimmerContainer2();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        children: [
          
          Container(
            width: 0.9 * MediaQuery.of(context).size.width,
            height: 0.2 * MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                color: Colors.grey, borderRadius: BorderRadius.circular(20)),
          ),
          
          
        ],
      ),
    );
  }
}


