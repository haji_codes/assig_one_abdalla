import 'package:assig_one_abdalla/fb_clone/data/data_export.dart';
import 'package:flutter/material.dart';

class NewPost extends StatelessWidget {
  const NewPost({super.key});

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.fromLTRB(12, 8, 12, 5),
        color: Colors.white,
        child: Column(children: [
          Row(
            children: [
              CircleAvatar(
                radius: 20,
                backgroundColor: Colors.grey,
                backgroundImage: AssetImage(currentUser.imgUri)
              ),
              const SizedBox(width: 8,),
              const Expanded(
                child:TextField(
                  decoration: InputDecoration.collapsed(hintText: 'What\'s on your mind?'),
                  
                ))
            ],
          ),
          const Divider(thickness: 0.5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextButton.icon(
                onPressed: (){},
                icon: const Icon(Icons.videocam,color: Colors.red,),
                label: const Text('Live',style: TextStyle(color: Colors.black),),
              ),
              TextButton.icon(
                onPressed: (){},
                icon: const Icon(Icons.photo_library,color: Colors.green,),
                label: const Text('Photo',style: TextStyle(color: Colors.black),),
              ),
              TextButton.icon(
                onPressed: (){},
                icon: const Icon(Icons. video_call,color: Colors.purple,),
                label: const Text('Room',style: TextStyle(color: Colors.black),),
              ),
            ],
          )
        ],),
      ),
    );
  }
}