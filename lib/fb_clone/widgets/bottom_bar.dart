import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({super.key});

  @override
  Widget build(BuildContext context) {
    return  DefaultTabController(
      length: 6,
      child: TabBar(
        indicatorPadding: EdgeInsets.zero,
        indicatorSize: TabBarIndicatorSize.tab,
        indicator: const BoxDecoration(
          border: Border(top: BorderSide(color: Color(0xFF1777F2),width: 3,))
        ),
        tabs: [
          const Icon(Icons.home,size: 30.0,),
          const Icon(Icons.ondemand_video,size: 30.0,),
          Icon(MdiIcons.accountCircleOutline,size: 30.0,),
          Icon(MdiIcons.accountGroupOutline,size: 30.0,),
          Icon(MdiIcons.bellOutline,size: 30.0,),
          const Icon(Icons.menu,size: 30.0,),
      
        ],
        labelColor: const Color(0xFF1777F2),
        ),
    );
  }
}