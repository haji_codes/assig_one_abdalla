import 'package:assig_one_abdalla/fb_clone/models/user_model.dart';

class Post {
  User user;
  String timeAgo;
  bool haveImage;
  String? postImgUri;
  bool privacy;
  String text;
  int likes;
  bool liked;
  int comments;
  int shares;
  
  Post ({
    required this.user,
    required this.timeAgo,
    required this.haveImage,
    this.postImgUri,
    required this.privacy,
    required this.text,
    required this.likes,
    required this.liked,
    required this.comments,
    required this.shares
});

}