import 'dart:math';
import 'package:assig_one_abdalla/fb_clone/data/users_data.dart';
import 'package:assig_one_abdalla/fb_clone/models/post_model.dart';
import 'package:faker/faker.dart';



final List<Post> posts = generatePosts(5);



List<Post> generatePosts(int count) {
  
  List<Post> posts = [];

  for (int i = 0; i < count; i++) {
    bool haveImageRandom = i % 2 != 0 ? true : false;
    // bool haveImageRandom = Random().nextBool();
    final currentPostImgUri = haveImageRandom ? '../../../assets/post_pics/pstImg${i.toString()}.jpg' : null;

    Post post = Post(
      user: onlineUsers[i],
      timeAgo: '${Random().nextInt(60)+1}m',
      haveImage: haveImageRandom,
      postImgUri:currentPostImgUri,
      privacy: Random().nextBool() ,
      text: faker.lorem.sentences(Random().nextInt(6)+1).join(),
      likes: Random().nextInt(100)+1,
      liked: false,
      comments: Random().nextInt(45)+1,
      shares: Random().nextInt(75)+1,
    );

    posts.add(post);
  }

  return posts;
}

