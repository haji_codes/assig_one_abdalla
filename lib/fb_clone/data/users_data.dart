import 'package:assig_one_abdalla/fb_clone/models/user_model.dart';
import 'package:faker/faker.dart';

final User currentUser =
    User(name: 'Haji', imgUri: '../../../assets/profile_pics/crntUsr.jpg');

final List<User> onlineUsers = List.generate(
    5,
    (index) => User(
        name: faker.person.name(),
        imgUri: '../../../assets/profile_pics/prfPic${index.toString()}.jpg'
    ));
