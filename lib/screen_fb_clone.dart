import 'package:assig_one_abdalla/fb_clone/widgets/widgets_export.dart';
import 'package:flutter/material.dart';

class FbCloneScreen extends StatefulWidget {
  const FbCloneScreen({super.key});

  @override
  State<FbCloneScreen> createState() => _HomePageState();
}

class _HomePageState extends State<FbCloneScreen> {

  bool isLoaded = false;


@override
  void initState() {
    Future.delayed(
      const Duration(seconds: 2),
      () {
        setState(() {
          isLoaded = true;
        });
      },
    );

    super.initState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: !isLoaded ? const LoadingShimmer() :
      
      const CustomScrollView(
        slivers: [
          Header(),
          NewPost(),
          PostContainer()
        ],
      ),
      bottomNavigationBar: const BottomNavBar(),
      backgroundColor: Colors.grey[200],
    );
  }
}